aiohttp==1.3.3
async-timeout==1.1.0
chardet==2.3.0
multidict==2.1.4
packaging==16.8
pluggy==0.4.0
PyYAML==3.12
six==1.10.0
yarl==0.9.8
