=======
Message
=======

The :code:`SlackMessage` class located in the :code:`sirbot_plugin_slack.message`
module provide an easy way to create and format message for slack.

A :code:`SlackMessage` should be at least composed of a :code:`to`
(User or Channel) and :code:`text`.
